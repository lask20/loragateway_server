

var MAX_DATA  = 16;
var PREFIX  = 0x11;

var JOIN_GATEWAY  = 0;
var GATEWAY_ACCEPTED  = 1;
var CHECK_ALIVE  = 2;
var REPLY_ALIVE  = 3;
var SLEEP_TIME  = 4;
var REPLY_SLEEP_TIME  = 5;
var SEND_MESSAGE  = 6;
var REPLY_SEND_MESSAGE  = 7;


class parsePacket{ 
	constructor(){
		this.prefix;
		this.source;
		this.destination;
		this.type;
		this.seq;
		this.data;
		this.rssi;
	}

	setRSSI(rssi) {
		this.rssi = rssi;
	}

	parse(raw) {
		this.prefix = raw[0];
		this.source = raw.readUInt32LE(1);
		this.destination = raw.readUInt32LE(5);
		this.type = raw[9];
		this.seq = raw[10];
		this.data = raw.slice(11);
		if (this.prefix == PREFIX) {
			return true;
		}
		return false;
	}

	toBuffer() {
		var buffer = new Buffer(27);
		buffer[0] = PREFIX;
		buffer.writeUInt32LE(this.source,1);
		buffer.writeUInt32LE(this.destination,5);
		buffer[9] = this.type;
		buffer[10] = this.seq;
		if (this.type == SEND_MESSAGE) {
			buffer.write(this.data,11,16);
		}
		return buffer;
	}




}

module.exports = parsePacket;