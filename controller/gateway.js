var express = require('express')
var router = express.Router()

let GatewayModel = require('../models/gateway');
let moment = require('moment');
let generic = require('../generic');

let formidable = require("formidable");


router.get('', function (req, res) {
  GatewayModel.find({}, function(err, gateways) {
    res.render("pages/gateway",{gateways:gateways,moment:moment});
    if (err) throw err;});
});


router.post('/delete', function(req, res) {
  GatewayModel.findByIdAndRemove(req.body.id, function(err) {
    if (err) throw err;    
    res.redirect("/gateway");
  });
});



router.get('/add', function(req, res) {
  var genid = generic.random(1);
  GatewayModel.find({gateway_id:genid}, function(err, devices) {
    if (Object.keys(devices).length == 0) {
      res.render("pages/formGateway",{gateway:{gateway_id:genid},moment:moment});
    }
  });
  

});

router.post('/add', function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var newDevice = GatewayModel(fields);
    newDevice.save(function(err) {
      if (err) throw err;});
    res.redirect("/gateway");
  });
});

router.get('/edit/:id', function(req, res) {
  GatewayModel.findById(req.params.id, function(err, gateway) {
    if (err) throw err;
    res.render("pages/formGateway",{gateway:gateway,moment:moment});
  });

});

router.post('/edit/:id', function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    GatewayModel.findByIdAndUpdate(req.params.id,fields , function(err, gateway) {
      if (err) throw err;
      res.redirect("/gateway");
    });
  });
});

module.exports = router