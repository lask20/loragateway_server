var express = require('express')
var router = express.Router()

let AppicationModel = require('../models/app');
let moment = require('moment');
let generic = require('../generic');

let formidable = require("formidable");

// // middleware that is specific to this router
// router.use(function timeLog (req, res, next) {
//   console.log('Time: ', Date.now())
//   next()
// })

router.get('', function (req, res) {
  AppicationModel.find({}, function(err, apps) {
    res.render("pages/app",{apps:apps,moment:moment});
    if (err) throw err;});
});


router.post('/delete', function(req, res) {
  AppicationModel.findByIdAndRemove(req.body.id, function(err) {
    if (err) throw err;    
    res.redirect("/app");
  });
});



router.get('/add', function(req, res) {
    var genid = generic.random(3);
  AppicationModel.find({app_id:genid}, function(err, devices) {
    if (Object.keys(devices).length == 0) {
      res.render("pages/formApp",{app:{app_id:genid},moment:moment});
    }
  });

  

});

router.post('/add', function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var newDevice = AppicationModel(fields);
    newDevice.save(function(err) {
      if (err) throw err;});
    res.redirect("/app");
  });
});

router.get('/edit/:id', function(req, res) {
  AppicationModel.findById(req.params.id, function(err, app) {
    if (err) throw err;
    res.render("pages/formApp",{app:app,moment:moment});
  });

});

router.post('/edit/:id', function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    AppicationModel.findByIdAndUpdate(req.params.id,fields , function(err, app) {
      if (err) throw err;
      res.redirect("/app");
    });
  });
});



module.exports = router