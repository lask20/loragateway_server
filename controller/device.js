var express = require('express')
var router = express.Router()

let DeviceModel = require('../models/device');
let moment = require('moment');
let generic = require('../generic');

let formidable = require("formidable");



router.get('', function (req, res) {


  DeviceModel.find({}, function(err, devices) {
    res.render("pages/device",{devices:devices,moment:moment});
    if (err) throw err;});
});


router.post('/delete', function(req, res) {
  DeviceModel.findByIdAndRemove(req.body.id, function(err) {
    if (err) throw err;    
    res.redirect("/device");
  });
});



router.get('/add', function(req, res) {
  var genid = generic.random(2);
  DeviceModel.find({device_id:genid}, function(err, devices) {
    if (Object.keys(devices).length == 0) {
      res.render("pages/formDevice",{device:{device_id:genid},moment:moment});
    }
  });

});

router.post('/add', function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var newDevice = DeviceModel(fields);
    newDevice.save(function(err) {
      if (err) throw err;});
    res.redirect("/device");
  });
});

router.get('/edit/:id', function(req, res) {
  DeviceModel.findById(req.params.id, function(err, device) {
    if (err) throw err;
    res.render("pages/formDevice",{device:device,moment:moment});
  });

});

router.post('/edit/:id', function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    DeviceModel.findByIdAndUpdate(req.params.id,fields , function(err, device) {
      if (err) throw err;
      res.redirect("/device");
    });
  });
});


module.exports = router