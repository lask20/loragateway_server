<<<<<<< .merge_file_Kc8O2h
var express = require("express");
let app = express();
=======

let app = require('express')();
>>>>>>> .merge_file_s2rCKC
let server = require('http').Server(app);

var mosca = require('mosca');
// var broker = new mosca.Server({});
// var path = require("path");

let io = require('socket.io')(server, {
  path: '/socket/gateway',
  serveClient: true,
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
});

let bodyParser = require('body-parser');
let moment = require('moment');
let formidable = require("formidable");
let util = require('util');
let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/gateway',{useMongoClient: true});


let gateway = require('./gateway');
let generic = require('./generic');

let appHub = require('./appHub');

let GatewayModel = require('./models/gateway');
let DeviceModel = require('./models/device');

var parsePacket = require('./parsePacket');




let loraGateway = {};
global.things = {};

// global.things = {2561135463:12122};

var appController = require('./controller/app');
var deviceController = require('./controller/device');
var gatewayController = require('./controller/gateway');

app.use('/app', appController);
app.use('/device', deviceController);
app.use('/gateway', gatewayController);


app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(express.static(path.dirname(require.resolve("mosca")) + "/public"));




app.get('/', function (req, res) {
  res.render('pages/index');
});

app.get('/status/gateway', function (req, res) {
  res.render('pages/statusGateway',{gateways:loraGateway});
});

app.get('/status/device', function (req, res) {
  res.render('pages/statusDevice',{things:things});
});

app.post('/send', function (req, res) {
  var data = req.body;
  buf = Buffer.from(data.data);
  var destination = Number(data.destination);
  if (destination in global.things) {
    var gw = global.things[destination];

    console.log(gw);
    var packet = new parsePacket();
    packet.source = Number(data.source);
    packet.destination = destination;
    packet.data = buf;
    packet.type = 6;
    packet.prefix = 0x11;

    console.log("sssssssss");

    console.log(packet);
    loraGateway[gw].send(packet);
  }
  res.send();
});





io.use((socket, next) => {
  let clientId = socket.handshake.headers['x-clientid'];
    if (isNaN(clientId)) {
       return next(new Error('authentication error'));
  }
  GatewayModel.find({gateway_id:clientId}, function(err, devices) {
    if (Object.keys(devices).length != 0) {
      socket.clientId = clientId;
      return next();
    }
    else {
      return next(new Error('authentication error'));
    }
  });
});

io.on('connection', function (socket) {
	console.log(socket.clientId);
	if (!(socket.clientId in loraGateway)) {
		loraGateway[socket.clientId] = new gateway(socket);
      socket.on('disconnect', (reason) => {
    loraGateway[socket.clientId].destroyThings();
    delete loraGateway[socket.clientId];
    console.log(Object.keys(loraGateway));
  });
	}
});
  

var ascoltatore = {
  type: 'redis',
  redis: require('redis'),
  db: 12,
  port: 6379,
  return_buffers: true, // to handle binary payloads
  host: "localhost"
};

// var ascoltatore = {
//   //using ascoltatore
//   type: 'mongo',
//   url: 'mongodb://localhost:27017/mqtt',
//   pubsubCollection: 'ascoltatori',
//   mongo: {}
// };

var moscaSettings = {
  port: 1883,
  backend: ascoltatore,
  persistence: {
    factory: mosca.persistence.Redis
  }
};

var settings = {
  port: 1883
};

var serverMosca = new mosca.Server(moscaSettings);

serverMosca.on('ready', setup);

// Accepts the connection if the username and password are valid
var authenticate = function(client, username, password, callback) {

      DeviceModel.find({device_id:client.id}, (err, devices) =>  {
      if (Object.keys(devices).length == 1) {
         callback(null, true);
      }
      else {
         callback(null, false);
      }
    });
}

// In this case the client authorized as alice can publish to /users/alice taking
// the username from the topic and verifing it is the same of the authorized user
var authorizePublish = function(client, topic, payload, callback) {
  callback(null, client.id == topic.split('/')[0]);
}

// In this case the client authorized as alice can subscribe to /users/alice taking
// the username from the topic and verifing it is the same of the authorized user
var authorizeSubscribe = function(client, topic, callback) {
  callback(null, client.id == topic.split('/')[1]);
}


// fired when a message is published
serverMosca.on('published', function(packet, client) {
console.log('Published', packet.topic, packet.payload);

    var type = Math.floor(parseInt(packet.topic.split('/')[1])/1000000000);
    switch(type) {
      case 0: 
      break;
      case 1:
      break;
      case 2:
      break;
      case 3:
        appHub.sendMq(parseInt(packet.topic.split('/')[1]),packet);
      break;
    }
});

// fired when a client connects
serverMosca.on('clientConnected', function(client) {
console.log('Client Connected:', client.id);
});

// fired when a client disconnects
serverMosca.on('clientDisconnected', function(client) {
console.log('Client Disconnected:', client.id);
});

// fired when the mqtt server is ready
function setup() {
  serverMosca.authenticate = authenticate;
  serverMosca.authorizePublish = authorizePublish;
  serverMosca.authorizeSubscribe = authorizeSubscribe;
  console.log('Mosca server is up and running')
}

server.listen(4000);


