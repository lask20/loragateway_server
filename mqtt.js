
var mosca = require('mosca');

var ascoltatore = {
  type: 'redis',
  redis: require('redis'),
  db: 12,
  port: 6379,
  return_buffers: true, // to handle binary payloads
  host: "localhost"
};

var moscaSettings = {
  port: 1883,
  backend: ascoltatore,
  persistence: {
    factory: mosca.persistence.Redis
  }
};

var settings = {
  port: 1883,
  backend: ascoltatore
};

var serverMosca = new mosca.Server(moscaSettings);
serverMosca.on('ready', setup);


// fired when a message is published
serverMosca.on('published', function(packet, client) {
console.log('Published', packet.topic, packet.payload);
});
// fired when a client connects
serverMosca.on('clientConnected', function(client) {
console.log('Client Connected:', client.id);
});

// fired when a client disconnects
serverMosca.on('clientDisconnected', function(client) {
console.log('Client Disconnected:', client.id);
});

// fired when the mqtt server is ready
function setup() {
  console.log('Mosca server is up and running')
}