var request = require('request');
var AppicationModel = require('./models/app');

var headers = {
	'User-Agent':       'Super Agent/0.0.1',
	'Content-Type':     'application/x-www-form-urlencoded'
}

function sendRequest(url,data) {
		var options = {
			url: url,
			method: 'POST',
			headers: headers,
			form: data
		}
		request(options, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				console.log(body);
			}
		});
	}

function sendApp(data) {
	AppicationModel.findOne({app_id:data.destination}, function(err, app) {
		if (app !== null) {
			console.log("sendd");
			var json = JSON.stringify(data);
			delete data["prefix"];
			delete data["seq"];
			delete data["type"];
			data.data = JSON.stringify(data.data);
			console.log(data);
			sendRequest(app.url,data);
		}
  });
}
function sendMq(destination,data) {
	AppicationModel.findOne({app_id:destination}, function(err, app) {
		if (app !== null) {
			sendRequest(app.url,data);
		}
  });
}



module.exports = {
	send:sendApp,
	sendMq:sendMq

}