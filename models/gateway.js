// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var schema = new Schema({
  gateway_id: { type: Number, required: true, unique: true },
  gateway_token: String,
  created_at: Date,
  last_connected_at: Date
});

// on every save, add the date
schema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  // this.last_connected_at = currentDate;

    if (!this.created_at)
  	this.created_at = currentDate;


  next();
});

// the schema is useless so far
// we need to create a model using it
var Gateway = mongoose.model('Gateway', schema);

// make this available to our users in our Node applications
module.exports = Gateway;