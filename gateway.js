var aesjs = require('aes-js');

let appHub = require('./appHub');
let DeviceModel = require('./models/device');


var key_128 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
var key = new Buffer(key_128);

class gateway{ 
	constructor(socket){
		this._socket = socket;
		this.id = socket.clientId;
		this.rx = 0;
		this.tx = 0;
		this.things = [];
		this.lastActive;
		this.connectAt = Date.now();
		
		this._socket.on("join",this._thingJoin.bind(this));
		this._socket.on("data",this._thingData.bind(this));

	}

	destroyThings() {
		this.things.forEach((item,index) => {
			delete global.things[item];
		});
	}

	send(data) {
		if (this._hasThing(data.destination)) {
			this._socket.emit("data",data);
		}
	}

	_thingJoin(data,fn) {
		global.things[data] = this.id;
		DeviceModel.find({device_id:data}, (err, devices) =>  {
			if (Object.keys(devices).length == 1) {
				fn({accepted:true});
				this._addThing(data);
			}
			else {
				fn({accepted:false});
			}
		});
		
	}
	_hasThing(id) {
		if (this.things.indexOf(id) == -1) {
			return false;
		}
		return true;
	}

	_addThing(id) {
		if (this.things.indexOf(id) == -1) {
			this.things.push(id);
		}
	}


	_removeThings(id) {
		if (this.things.indexOf(id) != -1) {
			this.things.pop(id);
		}

	}
	_thingData(data) {
		console.log(data);
		this._routing(data);
		// var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
		// var decryptedBytes = aesCtr.decrypt(data.data);
		// console.log(decryptedBytes);
	}

	_routing(data) {
		var type = Math.floor(data.destination/1000000000);
		switch(type) {
			case 0:	
			break;
			case 1:
			break;
			case 2:
			break;
			case 3:
				appHub.send(data);
			break;
		}
	}

}

module.exports = gateway;